package br.edu.unipampa.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "singularidade")
public class Singularidade {

	//private @Id @GeneratedValue(strategy = GenerationType.AUTO) Integer idSingularidade;

	private @Id @GeneratedValue(generator = "increment") @GenericGenerator(name = "increment",strategy = "increment") Integer idSingularidade;
	private String nomeSingularidade;

	public Singularidade() {
		super();
	}

	public Integer getIdSingularidade() {
		return idSingularidade;
	}

	public void setIdSingularidade(Integer idSingularidade) {
		this.idSingularidade = idSingularidade;
	}

	public String getNomeSingularidade() {
		return nomeSingularidade;
	}

	public void setNomeSingularidade(String nomeSingularidade) {
		this.nomeSingularidade = nomeSingularidade;
	}
}
