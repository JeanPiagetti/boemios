package br.edu.unipampa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.unipampa.model.Singularidade;

public interface SingularidadeRepository extends JpaRepository<Singularidade,Integer> {

}
