package br.edu.unipampa.controller;

import java.net.URI;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.edu.unipampa.model.Singularidade;
import br.edu.unipampa.repository.SingularidadeRepository;

@RestController
@RequestMapping("api/singularidade")
public class ControllerSingularidade {
	@Autowired
	private SingularidadeRepository repositorio;

	@GetMapping("retornarSingularidade")
	public List<Singularidade> listarLocais() {
		return  repositorio.findAll();
		
	}

	@PostMapping("inserirSingularidade")
	public ResponseEntity<Singularidade> inserir(@RequestBody Singularidade singularidade,
			HttpServletResponse response) {
		Singularidade singularidadeSalva = repositorio.save(singularidade);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("{/idSingularidade}")
				.buildAndExpand(singularidadeSalva.getIdSingularidade()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		return ResponseEntity.ok().body(singularidadeSalva);
	}

	@DeleteMapping("remover/{codigo}")
	public ResponseEntity<Void> remover(@PathVariable Integer codigo) {
		Singularidade singularidade = repositorio.getOne(codigo);
		if (singularidade == null) {
			return ResponseEntity.notFound().build();
		}
		repositorio.delete(singularidade);
		return ResponseEntity.noContent().build();
	}

	@PutMapping("atualizar/{codigo}")
	public ResponseEntity<Singularidade> atualizar(@PathVariable Integer codigo,
			@Valid @RequestBody Singularidade singularidade) {
		Singularidade singularidadeSalva = repositorio.getOne(codigo);
		if (singularidadeSalva == null) {
			return ResponseEntity.notFound().build();
		} else {
			return ResponseEntity.ok().build();
		}

	}
}
