package br.edu.unipampa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication 
public class BoemixsApiApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(BoemixsApiApplication.class, args);
	}

}
