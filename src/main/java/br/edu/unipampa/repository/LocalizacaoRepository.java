package br.edu.unipampa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.edu.unipampa.model.Localizacao;

public interface LocalizacaoRepository extends JpaRepository<Localizacao,Integer> {
	

}
