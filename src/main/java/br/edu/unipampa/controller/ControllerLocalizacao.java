package br.edu.unipampa.controller;

import java.io.File;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import br.edu.unipampa.model.Localizacao;
import br.edu.unipampa.repository.LocalizacaoRepository;

@RestController
@RequestMapping("/api")
@Component
public class ControllerLocalizacao {
	@Autowired
	private LocalizacaoRepository repositorio;

	@Value("${localizacao.disco.raiz}")
	private String RAIZ;
	// @Value("${localizacao.disco.diretorio-fotos}")
	private String PASTA = "\\fotos\\";

	private int keyIndex;

	@GetMapping
	public String index() {
		return "Index";
	}

	@GetMapping("/localizar")
	public List<Localizacao> getAll() {
		return repositorio.findAll();
	}

	@GetMapping(path = { "localizar/{id}" })
	public ResponseEntity<Localizacao> findById(@PathVariable int id) {
		return repositorio.findById(id).map(record -> ResponseEntity.ok().body(record))
				.orElse(ResponseEntity.notFound().build());
	}

//	@PostMapping(value = "/upload", consumes = { "multipart/form-data" })
//	public ResponseEntity<Localizacao> saveLocalFoto(@RequestPart("localizacao") Localizacao local,
//			@RequestPart("imagem") MultipartFile file) {
//		try {
//			Path caminho = null;
//
//			System.out.println("Imagem Local " + local.getImagem());
//
//			// se o arquivo existir, salva a imagem no diretorio escolhido
//			if (!file.isEmpty()) {
//				caminho = salvarFoto(file);
//			}
//
//			// salva as informacoes no banco de dados
//			Localizacao localizacaoSalva = repositorio.save(local);
//			localizacaoSalva.setImagem(RAIZ + PASTA + caminho.getFileName());
//
//			return ResponseEntity.ok().build();
//		} catch (Exception e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	
	@PostMapping(value = "inserirObjeto")
	public ResponseEntity<Localizacao> inserirObjeto(@RequestBody Localizacao localizacao,HttpServletResponse response ) {
		try {
			//tentar converter a String imagem para File imagem e armazenar a referencia dela no banco e o arquivo no sistema
			Localizacao localizacaoSalva = repositorio.save(localizacao);
			System.out.println("Imagem localizacao " + localizacao.getImage()); 
//			salvarFotoLocal(localizacao.getImage());
			URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("{/idLocalizacao}")
					.buildAndExpand(localizacaoSalva.getIdLocalizacao()).toUri();
			
			response.setHeader("Location", uri.toASCIIString());
	
			return ResponseEntity.ok().body(localizacaoSalva);
		}catch (Exception e) {
			return ResponseEntity.badRequest().build();
		}
		
	}
	
	@PostMapping("inserirLocal")
	public ResponseEntity<Localizacao> inserir(@RequestBody Localizacao local, HttpServletResponse response) {
		Localizacao localizacaoSalva = repositorio.save(local);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("{/idLocalizacao}").buildAndExpand(localizacaoSalva.getIdLocalizacao()).toUri();
		response.setHeader("Location", uri.toASCIIString());
		System.out.println("Localizacao Salva " + localizacaoSalva.getImage());
		return ResponseEntity.ok().body(localizacaoSalva);
	}
	
	
	private Path salvarFotoLocal(File arquivo) {
		Path diretorioPath = Paths.get(this.RAIZ,PASTA);
		Path caminhoArquivo =  diretorioPath.resolve(arquivo.getName());
		try{
			byte[] bytes = Files.readAllBytes(arquivo.toPath());
			System.out.println("Diretorio Path" + arquivo.getAbsolutePath());
			if(Files.notExists(diretorioPath)) {
				Files.createDirectories(diretorioPath);
			}if(!Files.exists(caminhoArquivo)) {
				Files.createFile(caminhoArquivo);
			}			
		}catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
//	
//	private Path salvarFoto(MultipartFile arquivo) throws Exception{
//		Path diretorioPath = Paths.get(this.RAIZ, PASTA);
//		Path arquivoPath = diretorioPath.resolve(arquivo.getOriginalFilename());
//		try {
//			byte[] bytes = arquivo.getBytes();
//			System.out.println("Diretorio Path " + diretorioPath);
//			System.out.println("Diretorio Arquivo " + arquivoPath);
//			if (Files.notExists(diretorioPath)) {
//				Files.createDirectories(diretorioPath);
//				
//			}
//			if (!Files.exists(arquivoPath)) {
//				Files.createFile(arquivoPath);
//			}
//
//			OutputStream bos = new FileOutputStream(arquivoPath.toFile().getAbsolutePath());
//			
//			bos.write(bytes);
//			bos.close();
////			Files.write(arquivoPath, bytes);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		
//		return arquivoPath;
//	}

	@PutMapping("atualizar/{codigo}")
	public ResponseEntity<Localizacao> atualizar(@PathVariable Integer codigo,
			@Valid @RequestBody Localizacao localizacao) {
		try {
			Localizacao localizacaoSalva = repositorio.getOne(codigo);
			if (localizacaoSalva == null) {
				return ResponseEntity.notFound().build();
			}
			BeanUtils.copyProperties(localizacao, localizacaoSalva, "codigo");
			repositorio.save(localizacao);
			return ResponseEntity.ok().build();
		} catch (BeansException e) {
			e.printStackTrace();
		}
		return null;
	}

	@DeleteMapping("remover/{codigo}")
	public ResponseEntity<Void> remover(@PathVariable Integer codigo) {
		try {
			Localizacao localizacao = repositorio.getOne(codigo);
			if (localizacao == null) {
				return ResponseEntity.notFound().build();
			}
			repositorio.delete(localizacao);
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
