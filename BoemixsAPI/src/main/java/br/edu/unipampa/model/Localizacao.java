package br.edu.unipampa.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.Transient;

@Entity
@Table(name = "localizacao")
public class Localizacao {

	//private @Id @GeneratedValue(strategy = GenerationType.AUTO) Integer idLocalizacao;
	private @Id @GeneratedValue(generator = "increment") @GenericGenerator(name = "increment",strategy = "increment") Integer idLocalizacao;

	private String nomeLocal;
	
	private @Column(length = 2000) String descricaoLocal;

	private @ManyToOne(cascade = CascadeType.DETACH, fetch = FetchType.EAGER) Singularidade singularidade;

	private @Column(length = 5000000) String image;
//	private @Transient String image;
	
	//Não grava no banco
	// objeto a ser inserido no banco de dados
	@Transient
	private byte[] imageData;
	
//	grava no banco
//	@Lob
//	private byte[] imageFull;
	
	private String contato;
	
//	private File image;

	public Localizacao(int idLocalizacao, String nomeLocal, String caminhoImg, Singularidade singularidade) {
		super();
	}

	public Localizacao() {
		super();
	}

//	
//	public File getImage() {
//		return image;
//	}
//
//	public void setImage(File image) {
//		this.image = image;
//	}

	public Integer getIdLocalizacao() {
		return idLocalizacao;
	}

	public Singularidade getSingularidade() {
		return singularidade;
	}

	public void setSingularidade(Singularidade singularidade) {
		this.singularidade = singularidade;
	}

	public void setIdLocalizacao(Integer idLocalizacao) {
		this.idLocalizacao = idLocalizacao;
	}

	public String getNomeLocal() {
		return nomeLocal;
	}

	public void setNomeLocal(String nomeLocal) {
		this.nomeLocal = nomeLocal;
	}

	public String getDescricaoLocal() {
		return descricaoLocal;
	}

	public void setDescricaoLocal(String descricaoLocal) {
		this.descricaoLocal = descricaoLocal;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getContato() {
		return contato;
	}

	public void setContato(String contato) {
		this.contato = contato;
	}

}